from fastapi import FastAPI, HTTPException, Query, status
import mysql.connector, uvicorn
from pydantic import BaseModel
from typing import List, Optional
from fastapi import Query


class MovieCreate(BaseModel):
    nom: str
    description: str
    date_parution: str
    note: int = None
    category_id: Optional[List[int]] = None

class CategoryCreate(BaseModel):
    name: str

class MoviePatch(BaseModel):
    nom: str = None
    description: str = None
    date_parution: str = None
    note: int = None
    category_id: Optional[List[int]] = None

class CategoryPatch(BaseModel):
    name: str

app = FastAPI()

# Connect to the MySQL server
conn = mysql.connector.connect(
    host='localhost',
    port=3306,
    user='root',
    password='YnovMysql',
    database='Ynov_Webservices'
)

@app.get("/films")
async def get_films_by_title_or_description(nom: str = Query(None, description="Filter films by name"), description: str = Query(None, description="Filter films by description")):
    try:
        # Create a cursor object to interact with the database
        cursor = conn.cursor(dictionary=True)

        # Build the SQL query based on the provided parameters
        query = "SELECT * FROM films WHERE 1=1"

        # Define the parameters
        params = {}

        if nom:
            query += " AND nom LIKE %(nom)s"
            params["nom"] = f"%{nom}%"
        if description:
            query += " AND description LIKE %(description)s"
            params["description"] = f"%{description}%"

        # Execute the query
        cursor.execute(query, params)

        # Fetch the films
        films = cursor.fetchall()

        # Close the cursor
        cursor.close()

        return films

    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")

@app.get("/categories")
async def get_all_categories():
    try:
        # Create a cursor object to interact with the database
        cursor = conn.cursor(dictionary=True)
        # Execute the query to get all categories
        cursor.execute("SELECT * FROM categories")
        # Fetch all the categories
        categories = cursor.fetchall()
        # Close the cursor
        cursor.close()
        return categories
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")

@app.get("/films/{film_id}")
async def get_a_film(film_id):
    try:
        # Create a cursor object to interact with the database
        cursor = conn.cursor(dictionary=True)
        # Execute the query to get a film
        cursor.execute(f"SELECT * FROM films WHERE ID={film_id}")
        # Fetch the the film
        films = cursor.fetchall()
        # Close the cursor
        cursor.close()
        if films == []:
            raise HTTPException(status_code=404, detail=f"No film found with id {film_id}")
        else:
            return films
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")

@app.get("/categories/{category_id}")
async def get_a_category(category_id):
    try:
        # Create a cursor object to interact with the database
        cursor = conn.cursor(dictionary=True)
        # Execute the query to get a film
        cursor.execute(f"SELECT * FROM categories WHERE ID={category_id}")
        # Fetch the the film
        categories = cursor.fetchall()
        # Close the cursor
        cursor.close()
        if categories == []:
            raise HTTPException(status_code=404, detail=f"No category found with id {category_id}")
        else:
            return categories
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")

@app.delete("/films/{film_id}")
async def delete_film(film_id: int):

    # create a new database session
    cursor = conn.cursor(dictionary=True)

    # get the film item with the given id
    cursor.execute(f"SELECT * FROM films WHERE ID={film_id}")

    films = cursor.fetchall()
    # if todo item with given id exists, delete it from the database. Otherwise raise 404 error
    if films != []:
        cursor.execute(f"DELETE FROM films WHERE ID={film_id}")
        return {'film deleted'}
    else:
        raise HTTPException(status_code=404, detail=f"film with id {id} not found")

@app.delete("/categories/{category_id}")
def delete_category(category_id: int):

    # create a new database session
    cursor = conn.cursor(dictionary=True)

    # get the film item with the given id
    cursor.execute(f"SELECT * FROM categories WHERE ID={category_id}")

    categories = cursor.fetchall()
    # if todo item with given id exists, delete it from the database. Otherwise raise 404 error
    if categories != []:
        cursor.execute(f"DELETE FROM categories WHERE ID={category_id}")
        return {'category deleted'}
    else:
        raise HTTPException(status_code=404, detail=f"category with id {id} not found")

@app.patch("/films/{film_id}")
async def patch_film(film_id: int, movie_data:MoviePatch):

    # create a new database session
    cursor = conn.cursor(dictionary=True)

    # get the film item with the given id
    cursor.execute(f"SELECT * FROM films WHERE ID={film_id}")

    films = cursor.fetchall()
    # if todo item with given id exists, delete it from the database. Otherwise raise 404 error
    if films != []:
        if movie_data.nom != None:
            cursor.execute(f"UPDATE films SET nom='{movie_data.nom}' WHERE id='{film_id}'")
        if movie_data.description != None:
            cursor.execute(f"UPDATE films SET description='{movie_data.description}' WHERE id='{film_id}'")
        if movie_data.date_parution != None:
            cursor.execute(f"UPDATE films SET date_parution='{movie_data.date_parution}' WHERE id='{film_id}'")
        if movie_data.note != None:
            cursor.execute(f"UPDATE films SET note='{movie_data.note}' WHERE id='{film_id}'")
        if movie_data.category_id:
            cursor.execute(
                f"UPDATE films SET category_id='{','.join(map(str, movie_data.category_id))}' WHERE id='{film_id}'"
            )

        return {'film updated'}
    else:
        raise HTTPException(status_code=404, detail=f"film with id {film_id} not found")

@app.patch("/categories/{category_id}")
def patch_category(category_id: int, category_data:CategoryPatch):

    # create a new database session
    cursor = conn.cursor(dictionary=True)

    # get the film item with the given id
    cursor.execute(f"SELECT * FROM categories WHERE ID={category_id}")

    categories = cursor.fetchall()
    # if todo item with given id exists, delete it from the database. Otherwise raise 404 error
    if categories != []:
        if category_data.name != None:
            cursor.execute(f"UPDATE categories SET name='{category_data.name}' WHERE id='{category_id}'")
            return {'categor patched'}
    else:
        raise HTTPException(status_code=404, detail=f"category with id {category_id} not found")
    
@app.post("/films", status_code=status.HTTP_201_CREATED)
async def add_movie(movie_data: MovieCreate):
    try:
        # Create a cursor object to interact with the database
        cursor = conn.cursor()

        # Execute the query to add the movie to the database
        cursor.execute(
            "INSERT INTO films (nom, description, date_parution, note, category_id) VALUES (%s, %s, %s, %s, %s)",
            (movie_data.nom, movie_data.description, movie_data.date_parution, movie_data.note, ",".join(map(str, movie_data.category_id)) if movie_data.category_id else None)
        )

        # Commit the changes
        conn.commit()

        # Close the cursor
        cursor.close()
        return {"message": "Movie added"}

    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")

@app.post("/categories", status_code=status.HTTP_201_CREATED)
async def add_categorie(category_data: CategoryCreate):
    try:
        # Create a cursor object to interact with the database
        cursor = conn.cursor()

        # Execute the query to add the category to the database
        cursor.execute(
            "INSERT INTO categories (name) VALUES (%s)",
            (category_data.name,)
        )

        # Commit the changes
        conn.commit()

        # Close the cursor
        cursor.close()
        return {"message": "category added"}

    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")

uvicorn.run(app)