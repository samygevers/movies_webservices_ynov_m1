# Movies_Webservices_Ynov_M1



Pour le lancer, il faut:
```
pip install mysql-connector, mysql-connector-python, uvicorn, fastapi
```
```
docker pull mysql:latest
```
```
docker run -d --name mysql-ynov -e MYSQL_ROOT_PASSWORD=YnovMysql -p 3306:3306 mysql:latest
```
```
docker exec -it mysql-ynov
```
```
mysql -u root -p
CREATE DATABASE Ynov_Webservices;
USE Ynov_Webservices;
```
mot de passe YnovMysql
```
CREATE TABLE IF NOT EXISTS films (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(128) NOT NULL,
    description TEXT NOT NULL,
    date_parution DATE NOT NULL,
    note INT CHECK (note >= 0 AND note <= 5),
    category_id VARCHAR(128)
);
```
```
CREATE TABLE IF NOT EXISTS categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(128) NOT NULL
    );
```
```
python API.py
```

les routes accessibles sont les plus classiques, voir la doc au format openAPI sur localhost:8000/docs

## CRUD
Il y a un CRUD pour les catégories & un pour les films 

## Catégories
Utilisant mySQL, nous avons faire un choix entre sérialiser les données ou créer une table intermédiaire pour lier les films aux catégories, c'est en la sérialisation des données qui à été choisi

## Recherche
Il faut passer le nom du film ou la description en paramètre
get /films?nom=toto

## A faire si on a le temps
Docker compose focnctionnel (bug chez moi???)  
Séparation du code - revoir archi   
Meilleure doc   
Validation des données de l'API   

Réalisé par Samy GEVERS et Florent ESTRAT