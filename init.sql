USE Ynov_Webservices;

CREATE TABLE IF NOT EXISTS films (
  id INT AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(128) NOT NULL,
  description TEXT NOT NULL,
  date_parution DATE NOT NULL,
  note INT CHECK (note >= 0 AND note <= 5)
  category_id VARCHAR(128)
);

CREATE TABLE IF NOT EXISTS categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(128) NOT NULL
    );


INSERT INTO films (nom, description, date_parution, note) VALUES
('Inception', 'Un film de science-fiction écrit et réalisé par Christopher Nolan.', '2010-07-16', 5),
('La La Land', 'Un film musical romantique écrit et réalisé par Damien Chazelle.', '2016-12-09', 4),
('The Shawshank Redemption', 'Un film dramatique basé sur une nouvelle de Stephen King, réalisé par Frank Darabont.', '1994-09-23', 5),
('The Dark Knight', 'Un film de super-héros réalisé par Christopher Nolan.', '2008-07-18', 5),
('Pulp Fiction', 'Un film de Quentin Tarantino avec une structure narrative non linéaire.', '1994-10-14', 4),
('Interstellar', 'Un film de science-fiction réalisé par Christopher Nolan.', '2014-11-07', 4),
('Forrest Gump', 'Un film dramatique basé sur le roman du même nom, réalisé par Robert Zemeckis.', '1994-07-06', 4),
('Titanic', 'Un film romantique écrit et réalisé par James Cameron.', '1997-12-19', 3),
('The Godfather', 'Un film de gangsters réalisé par Francis Ford Coppola, adapté du roman de Mario Puzo.', '1972-03-15', 5),
('The Matrix', 'Un film de science-fiction écrit et réalisé par les Wachowski.', '1999-03-31', 4),
('The Lord of the Rings: The Fellowship of the Ring', 'Un film d aventure fantastique réalisé par Peter Jackson.', '2001-12-19', 5),
('The Social Network', 'Un film biographique sur la création de Facebook, réalisé par David Fincher.', '2010-09-24', 4),
('Eternal Sunshine of the Spotless Mind', 'Un film de science-fiction romantique réalisé par Michel Gondry.', '2004-03-19', 4),
('The Silence of the Lambs', 'Un thriller psychologique réalisé par Jonathan Demme, adapté du roman de Thomas Harris.', '1991-02-14', 5),
('Schindler''s List', 'Un film dramatique basé sur le roman de Thomas Keneally, réalisé par Steven Spielberg.', '1993-11-30', 5),
('Avatar', 'Un film de science-fiction écrit et réalisé par James Cameron.', '2009-12-18', 3),
('The Grand Budapest Hotel', 'Un film comique réalisé par Wes Anderson.', '2014-02-06', 4),
('Jurassic Park', 'Un film de science-fiction réalisé par Steven Spielberg, adapté du roman de Michael Crichton.', '1993-06-11', 4),
('Fight Club', 'Un film de David Fincher basé sur le roman de Chuck Palahniuk.', '1999-10-15', 4),
('The Departed', 'Un film policier réalisé par Martin Scorsese.', '2006-10-06', 5),
('Inglourious Basterds', 'Un film de guerre écrit et réalisé par Quentin Tarantino.', '2009-08-21', 4),
('The Revenant', 'Un film d aventure dramatique réalisé par Alejandro González Iñárritu.', '2015-12-25', 4),
('The Great Gatsby', 'Un film romantique-dramatique réalisé par Baz Luhrmann.', '2013-05-10', 3),
('Memento', 'Un thriller psychologique réalisé par Christopher Nolan.', '2000-09-05', 4),
('The Green Mile', 'Un film dramatique basé sur le roman de Stephen King, réalisé par Frank Darabont.', '1999-12-10', 5);

SELECT * FROM films;